CREATE TABLE `tx_domaincheck_domain_model_tld`
(
	`uid`              INT(11)             NOT NULL AUTO_INCREMENT,
	`pid`              INT(11)             NOT NULL DEFAULT '0',
	`tstamp`           INT(11) UNSIGNED    NOT NULL DEFAULT '0',
	`crdate`           INT(11) UNSIGNED    NOT NULL DEFAULT '0',
	`cruser_id`        INT(11) UNSIGNED    NOT NULL DEFAULT '0',
	`deleted`          TINYINT(4) UNSIGNED NOT NULL DEFAULT '0',
	`hidden`           TINYINT(4) UNSIGNED NOT NULL DEFAULT '0',
	`tld`              TEXT                NOT NULL,
	`regex_check`      TEXT                NOT NULL,
	`whois`            INT(11)             NOT NULL DEFAULT '0',
	`editlock`         TINYINT(4) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`uid`),
	KEY `parent` (`pid`)
);

CREATE TABLE `tx_domaincheck_domain_model_whois`
(
	`uid`              INT(11)             NOT NULL AUTO_INCREMENT,
	`pid`              INT(11)             NOT NULL DEFAULT '0',
	`tstamp`           INT(11) UNSIGNED    NOT NULL DEFAULT '0',
	`crdate`           INT(11) UNSIGNED    NOT NULL DEFAULT '0',
	`cruser_id`        INT(11) UNSIGNED    NOT NULL DEFAULT '0',
	`deleted`          TINYINT(4) UNSIGNED NOT NULL DEFAULT '0',
	`hidden`           TINYINT(4) UNSIGNED NOT NULL DEFAULT '0',
	`server`           TEXT                NOT NULL,
	`idn`              INT(11)             NOT NULL DEFAULT '0',
	`regex_registered` TEXT                NOT NULL,
	`regex_free`       TEXT                NOT NULL,
	`regex_invalid`    TEXT                NOT NULL,
	`editlock`         TINYINT(4) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`uid`),
	KEY `parent` (`pid`)
);
