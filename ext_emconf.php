<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "domaincheck".
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Domain check',
    'description' => 'Frontend plugin to check domains.',
    'category' => 'plugin',
    'version' => '2.0.0',
    'state' => 'stable',
    'author' => 'Alexander Grein',
    'author_email' => 'alexander.grein@gmail.com',
    'author_company' => 'MEDIA::ESSENZ',
    'constraints' =>
        [
            'depends' =>
                [
                    'typo3' => '11.5.21-12.4.99',
                    'typoscript_rendering' => '2.0.0-',
                ],
            'conflicts' =>
                [],
            'suggests' =>
                [],
        ],
    'autoload' => [
        'psr-4' => [
            'MEDIAESSENZ\\Domaincheck\\' => 'Classes',
        ],
    ],
];
