<?php

namespace MEDIAESSENZ\Domaincheck\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\Repository;

class TldRepository extends Repository
{
    /**
     * @param array $uids
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByUids($uids)
    {
        $query = $this->createQuery();
        $query->matching($query->in('uid', $uids));

        return $query->execute();
    }
}
