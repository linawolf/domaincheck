<?php

namespace MEDIAESSENZ\Domaincheck\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Class Whois
 * @package MEDIAESSENZ\Domaincheck\Domain\Model
 */
class Whois extends AbstractEntity
{
    /**
     * @var string
     */
    protected $server;

    /**
     * @var int
     */
    protected $idn;

    /**
     * @var string
     */
    protected $regexRegistered;

    /**
     * @var string
     */
    protected $regexFree;

    /**
     * @var string
     */
    protected $regexInvalid;

    /**
     * @return string
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * @param string $server
     * @return Whois
     */
    public function setServer($server)
    {
        $this->server = $server;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdn()
    {
        return $this->idn;
    }

    /**
     * @param int $idn
     * @return Whois
     */
    public function setIdn($idn)
    {
        $this->idn = $idn;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegexRegistered()
    {
        return $this->regexRegistered;
    }

    /**
     * @param string $regexRegistered
     * @return Whois
     */
    public function setRegexRegistered($regexRegistered)
    {
        $this->regexRegistered = $regexRegistered;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegexFree()
    {
        return $this->regexFree;
    }

    /**
     * @param string $regexFree
     * @return Whois
     */
    public function setRegexFree($regexFree)
    {
        $this->regexFree = $regexFree;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegexInvalid()
    {
        return $this->regexInvalid;
    }

    /**
     * @param string $regexInvalid
     * @return Whois
     */
    public function setRegexInvalid($regexInvalid)
    {
        $this->regexInvalid = $regexInvalid;
        return $this;
    }
}
