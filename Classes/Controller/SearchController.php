<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Domaincheck\Controller;

use Psr\Http\Message\ResponseInterface;

class SearchController extends AbstractController
{
    public function searchAction(): ResponseInterface
    {
        $this->view->assign('data', $this->configurationManager->getContentObject()->data);
        $this->view->assign('tlds', $this->getTlds($this->settings['tlds']));

        return $this->htmlResponse();
    }

}
