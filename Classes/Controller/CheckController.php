<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Domaincheck\Controller;

use JsonException;
use MEDIAESSENZ\Domaincheck\Domain\Model\Tld;
use MEDIAESSENZ\Domaincheck\Exception\CallFailedException;
use MEDIAESSENZ\Domaincheck\Utility\WhoisUtility;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Mvc\View\JsonView;

/**
 * Class Check
 * @package MEDIAESSENZ\Domaincheck\Controller
 */
class CheckController extends AbstractController
{
    /**
     * @var string
     */
    protected $defaultViewObjectName = JsonView::class;

    /**
     * @throws CallFailedException
     * @throws NoSuchArgumentException
     * @throws JsonException
     */
    public function checkAction(): ResponseInterface
    {
        $check = $this->request->getArgument('check');
        if ($check['tld']) {
            $this->settings['tlds'] = implode(',', $check['tld']);
        }
        $tlds = $this->getTlds($this->settings['tlds']);

        $sld = strtolower(trim($check['sld']));

        // remove www. if set
        if (str_starts_with($sld, 'www.')) {
            $sld = substr($sld, 4);
        }

        // remove .tld from sld if set
        if ($pos = strpos($sld, '.')) {
            $sld = substr($sld, 0, $pos);
        }

        $whoisResponse = [];

        if ($this->extConf['inwx_domain_robot_service']['username'] && $this->extConf['inwx_domain_robot_service']['password']) {
            // If INWX username and password is set try to connect to INWX
            try {
                $inwxDomainRobotServiceConnection = $this->inwxDomainRobotService
                    ->setLanguage($this->extConf['inwx_domain_robot_service']['language'])
                    ->useLive()
                    ->useJson()
                    ->login($this->extConf['inwx_domain_robot_service']['username'], $this->extConf['inwx_domain_robot_service']['password'], $this->extConf['inwx_domain_robot_service']['sharedSecret']);

            } catch (CallFailedException $callFailedException) {
                $this->view->assign('value', [
                    'result' => 'ERROR',
                    'message' => $this->debug ? $callFailedException->getMessage() : 'Set plugin.tx_domaincheck.settings.debug = 1 to see error messages'
                ]);

                return $this->jsonResponse();
            }

            if ((int)($inwxDomainRobotServiceConnection['code'] ?? 0) === 1000) {
                foreach ($tlds as $tld) {
                    /** @var Tld $tld */
                    $domain = strtolower($sld . '.' . $tld->getTld());
                    try {
                        $result = $this->inwxDomainRobotService->call('domain', 'check',  ['domain' => $domain]);
                        $whoisResponse[] = [
                            'domain' => $domain,
                            'status' => $result['resData']['domain'][0]['status']
                        ];
                    } catch (CallFailedException $callFailedException) {
                        $this->view->assign('value', [
                            'result' => 'ERROR',
                            'message' => $this->debug ? $callFailedException->getMessage(): 'Set plugin.tx_domaincheck.settings.debug = 1 to see error messages'
                        ]);

                        return $this->jsonResponse();
                    }
                }
            }
            $this->inwxDomainRobotService->logout();
        } else {
            $whoisResponse = WhoisUtility::checkDomains(
                $sld,
                $tlds,
                $this->extConf['whois_command'],
                $this->extConf['proxy']
            );
        }

        $orderPage = $this->uriBuilder->setTargetPageUid((int)$this->settings['orderPage'])->setCreateAbsoluteUri(true)->build();

        $this->view->assign('value', [
            'result' => 'OK',
            'whoisResponse' => $whoisResponse,
            'orderPage' => $orderPage
        ]);

        return $this->jsonResponse();
    }

}
