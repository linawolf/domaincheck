<?php
namespace MEDIAESSENZ\Domaincheck\Hooks;

use TYPO3\CMS\Form\Domain\Model\Renderable\RenderableInterface;

class FormElementHooks
{
    /**
     * @param RenderableInterface $renderable
     * @return void
     */
    public function afterBuildingFinished(RenderableInterface $renderable)
    {
        if (method_exists($renderable, 'getRootForm') && method_exists($renderable->getRootForm(), 'getIdentifier')) {
            $rootFormIdentifier = $renderable->getRootForm()->getIdentifier();
            if (strstr($rootFormIdentifier, 'ext-domaincheck-registration') !== false && $domain = $_GET['domain']) {
                $identifier = $renderable->getIdentifier();
                if ($identifier === 'domainname') {
                    $renderable->setLabel($domain);
                }
                if ($identifier === 'domain') {
                    $renderable->setDefaultValue($domain);
                }
            }
        }
    }

}
