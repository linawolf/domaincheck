<?php

namespace MEDIAESSENZ\Domaincheck\Utility;

class CheckUtility
{
    /**
     * @param string $response
     * @param array $whois
     * @param array $marker
     * @return string
     */
    public static function checkResponse($response, $whois, $marker): string
    {
        foreach ($marker as &$mark) {
            $mark = preg_quote($mark);
        }

        if (preg_match('/' . strtr($whois['regex_free'], $marker) . '/i', $response)) {
            return 'free';
        }
        if (!empty($whois['regex_invalid']) && preg_match('/' . strtr($whois['regex_invalid'], $marker) . '/i',
                $response)) {
            return 'sld_invalid';
        }
        if (empty($whois['regex_registered']) || (preg_match('/' . strtr($whois['regex_registered'],
                    $marker) . '/i', $response))) {
            return 'used';
        }

        return 'tld_invalid';
    }
}
