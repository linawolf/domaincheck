const getLabel = function (key) {
    if (typeof TYPO3.lang[key] !== 'undefined') {
        return TYPO3.lang[key];
    } else {
        return '';
    }
};

const checkDomain = function (form, goButton) {
    const l = Ladda.create(goButton);
    l.start();

    // Handle form submission
    // Get the form action and serialize the form data
    const url = form.action;
    const formData = new FormData(form);

    // Create an AJAX request using the POST method
    const xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);

    // Set the request headers
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    // Handle response
    xhr.onload = function () {
        if (xhr.status === 200) {
            const response = JSON.parse(xhr.responseText);

            if (response.result === 'OK') {
                let result = '<div class="tx-domaincheck-search-result"><h2>' + getLabel('whois_response_result') + '</h2><div class="table-responsive"><table class="table table-striped">';
                const separator = response.orderPage.indexOf('?') !== -1 ? '&' : '?';

                response.whoisResponse.forEach(function (value, index) {
                    if (value.status === 'free') {
                        result += '<tr><td>' + getLabel('whois_response_available').replace('%', value.domain) + '</td><td class="text-right"><a href="' + response.orderPage + separator + 'domain=' + encodeURIComponent(value.domain) + '" class="btn btn-success">' + getLabel('order_btn') + ' <span class="fas fa-check"></span></a></td></tr>';
                    } else {
                        result += '<tr><td>' + getLabel('whois_response_occupied').replace('%', value.domain) + '</td><td class="text-right"><a class="btn btn-danger" href="http://www.' + value.domain + '" target="_blank">' + getLabel('occupied_btn') + ' <span class="fas fa-times"></span></a></td></tr>';
                    }
                });

                result += '</table></div></div>';

                // Insert the HTML result after the form element
                form.insertAdjacentHTML('afterend', result);
            }
        }

        // Stop loading indicator
        l.stop();
    };

    // Send the request with the serialized form data
    xhr.send(new URLSearchParams(formData));

    // Start loading indicator
    l.start();

    // Return false to prevent default form submission behavior
    return false;
};

document.addEventListener('DOMContentLoaded', function () {

    const domainCheckSearchForms = [...document.querySelectorAll('.tx-domaincheck-search-form')];
    if (domainCheckSearchForms.length > 0) {
        domainCheckSearchForms.forEach(function (domainCheckSearchForm) {
            const uid = domainCheckSearchForm.dataset.uid;
            const goButton = document.getElementById('tx-domaincheck-search-go-' + uid);
            document.getElementById('tx-domaincheck-sld-' + uid).addEventListener('keyup', function (event) {
                goButton.disabled = event.target.value.length < 2;
            });

            domainCheckSearchForm.addEventListener('submit', function (event) {
                event.preventDefault();
                if (domainCheckSearchForm.nextSibling.classList && domainCheckSearchForm.nextSibling.classList.contains('tx-domaincheck-search-result')) {
                    domainCheckSearchForm.nextSibling.remove();
                }
                checkDomain(domainCheckSearchForm, goButton);
            });
        });
    }
});
