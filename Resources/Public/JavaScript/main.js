;(function ($, window, document, undefined) {
    "use strict";

    var getLabel = function (key) {
        if (typeof TYPO3.lang[key] !== 'undefined') {
            return TYPO3.lang[key];
        } else {
            return '';
        }
    };

    $(function () {
        var domaincheck = function ($form, $btn) {
            var l = Ladda.create($btn);
            l.start();
            $.post($form.prop('action'), $.param($form.serializeArray()))
                .done(function (data) {
                    if (data.result === 'OK') {
                        var result = '<div class="tx-domaincheck-search-result"><h2>' + getLabel('whois_response_result') + '</h2><div class="table-responsive"><table class="table table-striped">';
                        var separator = data.orderPage.indexOf('?') !== -1 ? '&' : '?';
                        $.each(data.whoisResponse, function (index, value) {
                            if (value.status === 'free') {
                                result += '<tr><td>' + getLabel('whois_response_available').replace('%', value.domain) + '</td><td class="text-right"><a href="' + data.orderPage + separator + 'domain=' + encodeURIComponent(value.domain) + '" class="btn btn-success">' + getLabel('order_btn') + ' <span class="fas fa-check"></span></a></td></tr>';
                            } else {
                                result += '<tr><td>' + getLabel('whois_response_occupied').replace('%', value.domain) + '</td><td class="text-right"><a class="btn btn-danger" href="http://www.' + value.domain + '" target="_blank">' + getLabel('occupied_btn') + ' <span class="fas fa-times"></span></a></td></tr>';
                            }
                        });
                        result += '</table></div></div>';
                        $form.after(result);
                    }
                })
                .always(function () {
                    l.stop();
                });
            return false;
        };
        if ($('.tx-domaincheck-search-form').length > 0) {

            $('.tx-domaincheck-search-tlds').selectize({
                maxItems: 5,
                width: 300

            });


            $('.tx-domaincheck-search-form').each(function () {
                var $form = $(this);
                $('.tx-domaincheck-search-sld').on('keyup', function () {
                    $form.find('.tx-domaincheck-search-go').prop('disabled', $(this).val().length < 2);
                });
                $form.find('.tx-domaincheck-search-go').on('click', function (e) {
                    $form.nextAll('.tx-domaincheck-search-result').fadeOut(500, function () {
                        $(this).remove();
                    });
                    domaincheck($form, this);
                });
            });
        }
    });
})(jQuery, window, document);
