.. include:: /Includes.rst.txt

===========
domaincheck
===========

:Extension key:
   mail

:Package name:
   mediaessenz/domaincheck

:Version:
   |release|

:Language:
   en

:Author:
   Alexander Grein

:License:
   This document is published under the
   `Open Publication License <https://www.opencontent.org/openpub/>`__.

:Rendered:
   |today|

----

This extension check the availability of domains by using the API of INWX, whois command or a whois proxy.

----

.. container:: row m-0 p-0

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Introduction <introduction>`

         .. container:: card-body

            A quick introduction in how to use this extension.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Changelog <changelog>`

         .. container:: card-body

            Changelog


.. Table of Contents

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :hidden:

   Introduction/Index
   Changelog/Index

.. Meta Menu

.. toctree::
   :hidden:

   Sitemap
   genindex
